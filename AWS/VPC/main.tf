# Define a vpc
resource "aws_vpc" "TFVPC" {
  cidr_block = var.cidr_block_vpc
  tags = {
    Name = "${var.vpc_name}"
  }
}
# Internet gateway for the public subnet
resource "aws_internet_gateway" "TFIntGtw" {
  vpc_id = aws_vpc.TFVPC.id
  tags = {
    Name = "IGVPCTerraform"
  }
}
# Public subnet
resource "aws_subnet" "PubSubTFVPC" {
  vpc_id            = aws_vpc.TFVPC.id
  cidr_block        = var.cidr_block_sub
  availability_zone = var.region_name
  tags = {
    Name = "PublicSubnetTFVPC"
  }
}
# Routing table for public subnet
resource "aws_route_table" "PubSubTFVPCRoute" {
  vpc_id = aws_vpc.TFVPC.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.TFIntGtw.id
  }
  tags = {
    Name = "PublicSubnetTFVPCRoute"
  }
}
# Associate the routing table to public subnet
resource "aws_route_table_association" "PubSubTFVPCASS" {
  subnet_id      = aws_subnet.PubSubTFVPC.id
  route_table_id = aws_route_table.PubSubTFVPCRoute.id
}
# ECS Instance Security group

resource "aws_security_group" "TFSG" {
  name        = "TFSG"
  description = "Security group para realizar teste terraform"
  vpc_id      = aws_vpc.TFVPC.id
  
  ingress  {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = [
    "0.0.0.0/0"]
  }

  ingress  {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = [
    "0.0.0.0/0"]
  }

    ingress  {
    from_port = 8080
    to_port   = 8080
    protocol  = "tcp"
    cidr_blocks = [
    "0.0.0.0/0"]
  }  

  egress  {
    # allow all traffic to private SN
    from_port = "0"
    to_port   = "0"
    protocol  = "-1"
    cidr_blocks = [
    "0.0.0.0/0"]
  }

  tags = {
    Name = "${var.security_group_name}"
  }
}