variable "cidr_block_vpc" {
  default     = "19.10.0.0/16"
}

variable "cidr_block_sub" {
  default     = "19.10.0.0/24"
}

variable "vpc_name" {
  default     = "VPCTerraform"
}

variable "region_name" {
    default = "us-east-1"
}

variable "security_group_name" {
    default = "us-east-1"
}