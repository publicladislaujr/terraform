provider "aws" {
  region = var.region_name
}

resource "aws_instance" "linuxservices" {
  ami                    = var.ami_name
  instance_type          = var.instance_type
  key_name               = var.key_name
  count                  = var.qnt_value
  vpc_security_group_ids = var.security_group
  subnet_id              = count.index % 2 == 0 ? var.subnet_ids[1] : var.subnet_ids[0]
  root_block_device {
    volume_size           = "50"
    volume_type           = "gp2"
    encrypted             = true
    delete_on_termination = true
  }
  
  user_data = <<EOF
#!/bin/bash

echo "Changing Hostname"

if [[ ${count.index} -ge 9 ]] 
then  
  hostname "${var.host_name}""${count.index+1}"
  echo "${var.host_name}""${count.index+1}" > /etc/hostname
else 
  hostname "${var.host_name}""${count.index+1}"
  echo "${var.host_name}""${count.index+1}" > /etc/hostname
fi

yum update -y
yum upgrade -y

#Inicio - Pacote padrão Ec2
yum install nmap -y
yum install jwhois -y
yum install telnet -y
yum install nslookup -y
yum install whois -y
yum install bind-utils -y
yum install vim -y
yum install wget -y

#Desativa IPTABLES
iptables -I FORWARD -j ACCEPT

shutdown
  EOF

  tags = {
    Name = count.index+1 > 9 ? "${var.host_name}${count.index+1}" : "${var.host_name}0${count.index+1}"
    Environment = "${var.environment_value}"
  }
}