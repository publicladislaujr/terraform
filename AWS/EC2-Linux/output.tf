output "instances" {
  value       = "${aws_instance.linuxservices.*.private_ip}"
  description = "PrivateIP address details"
}