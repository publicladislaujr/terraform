variable "region_name" {
    default = "us-east-1"
}

variable "subnet_ids" {
  type = list(string)
  default = ["subnet-0000000000000000","subnet-0000000000000000"]
}

variable "instance_type" {
    default = "t2.small"
}

variable "ami_name" {
    default = "ami-0800f9916b7655289"
}

variable "key_name" {
    default = "nome_da_chave"
}

variable "qnt_value" {
    default = 12
}

variable "security_group" {
    default = ["sg-0000000000000000","sg-0000000000000000"]
}

variable "host_name" {
    default = "NOMEDOSERVIDOR"
}

variable "environment_value" {
    default = "PD"
}