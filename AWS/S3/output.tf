output "secret" {
  value = aws_iam_access_key.lb.encrypted_secret
}