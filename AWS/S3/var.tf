variable "environment_value" {
    default = "DEV"
}

variable "region_name" {
    default = "us-east-1"
}

variable "bucket_name" {
    default = "nomedobucket"
}

variable "acl_value" {
    default = "private"
}

variable "user_name" {
    default = "nomedousuario"
}

variable "policy_name" {
    default = "policyacesso"
}