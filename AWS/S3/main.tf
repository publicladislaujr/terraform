provider "aws" {
  region = var.region_name
}

resource "aws_s3_bucket" "b" {
    bucket = var.bucket_name     

  tags = {
    Name        = var.bucket_name
    Environment = var.environment_value
  }
}
resource "aws_s3_bucket_acl" "b" {
  bucket = aws_s3_bucket.b.id
  acl    = "private"
}

resource "aws_iam_user" "lb" {
  name = var.user_name
  path = "/system/"
  
}

resource "aws_iam_access_key" "lb" {
  user = aws_iam_user.lb.name
}

resource "aws_iam_user_policy" "lb_ro" {
  name = "${var.policy_name}${var.bucket_name}"
  user = aws_iam_user.lb.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "S3:*"
      ],
      "Effect": "Allow",
      "Resource": [
          "arn:aws:s3:::${var.bucket_name}",
          "arn:aws:s3:::${var.bucket_name}/*"    
      ]
    }
  ]
}
EOF
}
