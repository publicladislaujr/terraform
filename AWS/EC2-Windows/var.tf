variable "region_name" {
    default = "us-east-1"
}

variable "ami_name" {
    default = "ami-0eab1a8ed6837615a"
}

variable "instance_type" {
    default = "r5.2xlarge"
}

variable "subnet_id" {
    default = "subnet-0000000000000000"
}

variable "security_group" {
    default = ["sg-0000000000000000","sg-0000000000000000"]
}

variable "key_name" {
    default = "nome_da_chave"
}

variable "host_name" {
    default = "NOMEDOSERVIDOR"
}

variable "environment_value" {
    default = "PD"
}
