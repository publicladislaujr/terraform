provider "aws" {
  region = var.region_name
}

resource "aws_instance" "windows-server" {
  ami = var.ami_name
  instance_type = var.instance_type
  subnet_id = var.subnet_id
  vpc_security_group_ids = var.security_group
  source_dest_check = false
  key_name = var.key_name
    
  # root disk
  root_block_device {
    volume_size           = 100
    volume_type           = "gp2"
    delete_on_termination = true
    encrypted             = true
     tags = {
      Name        = "C: SO"
    }
  }
  # extra disks
  ebs_block_device {
    device_name           = "/dev/xvdb"
    volume_size           = 200
    volume_type           = "gp3"
    encrypted             = true
    delete_on_termination = true
    tags = {
      Name        = "B: Backup"
    }
  } 
   tags = {
    Name = "${var.host_name}"
    Environment = "${var.environment_value}"

  }
}

