output "instances" {
  value       = "${aws_instance.windows-server.*.private_ip}"
  description = "PrivateIP address details"
}